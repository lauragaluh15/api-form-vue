//import vue router
import { createRouter, createWebHistory } from "vue-router";

//define a routes
const routes = [
    {
        path: "/",
        name: "employee.index",
        component: () =>
            import(
                /* webpackChunkName: "employee.index" */ "@/views/employees/index.vue"
            ),
    },
    {
        path: "/create",
        name: "employee.create",
        component: () =>
            import(
                /* webpackChunkName: "employee.create" */ "@/views/employees/create.vue"
            ),
    },
    {
        path: "/edit/:id",
        name: "employee.edit",
        component: () =>
            import(/* webpackChunkName: "employee.edit" */ "@/views/employees/edit.vue"),
    },
];

//create router
const router = createRouter({
    history: createWebHistory(),
    routes, // config routes
});

export default router;
