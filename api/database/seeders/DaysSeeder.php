<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DaysSeeder extends Seeder
{
    public function run()
    {
        $days = array('Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu', 'Ahad');
        foreach ($days as $day) {
            DB::table('days')->insert([
                'day' => $day,
            ]);
        }
    }
}
