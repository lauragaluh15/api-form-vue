<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Day;

class DaysController extends Controller
{
    public function index()
    {
        $days = Day::all();
        return response()->json([
            'success' => true,
            'message' => 'days data',
            'data' => $days,
        ], 200);
    }
}
