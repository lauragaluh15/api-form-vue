<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Employee;
use Validator;

class EmployeesController extends Controller
{
    public function index()
    {
        $employees = Employee::orderBy('first_name', 'asc')->get();
        return response()->json([
            'success' => true,
            'message' => 'employees data',
            'data' => $employees,
        ], 200);
    }
    public function show($id)
    {
        $employee = Employee::findOrfail($id);
        return response()->json([
            'success' => true,
            'message' => 'employee details',
            'data'    => $employee
        ], 200);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'address' => 'required',
            'phone' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        $employee = Employee::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'address' => $request->address,
            'phone' => $request->phone,
            'salary' => $request->salary,
            'day_id' => $request->day_id,
        ]);
        if ($employee) {
            return response()->json([
                'success' => true,
                'message' => 'employee created',
                'data' => $employee
            ], 201);
        }
        return response()->json([
            'success' => false,
            'message' => 'employee failed to save',
        ], 409);
    }
    public function update(Request $request, Employee $employee)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'address' => 'required',
            'phone' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        $employee = Employee::findOrFail($employee->id);
        if ($employee) {
            $employee->update([
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'address' => $request->address,
                'phone' => $request->phone,
                'salary' => $request->salary,
                'day_id' => $request->day_id,
            ]);
            return response()->json([
                'success' => true,
                'message' => 'emplyee updated',
                'data'    => $employee
            ], 200);
        }
        return response()->json([
            'success' => false,
            'message' => 'employee not found',
        ], 404);
    }
    public function destroy($id)
    {
        $employee = Employee::findOrfail($id);
        if ($employee) {
            $employee->delete();
            return response()->json([
                'success' => true,
                'message' => 'employee deleted',
            ], 200);
        }
        return response()->json([
            'success' => false,
            'message' => 'employee not found',
        ], 404);
    }
}
