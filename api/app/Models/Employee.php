<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory;
    protected $table = 'employees';
    protected $fillable = [
        'first_name', 'last_name', 'address', 'phone', 'salary', 'day_id'
    ];
    public function day()
    {
        return $this->belongsTo('App\Models\Day');
    }
}
